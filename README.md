# FastAI course 1 version 3 (2018) docker environment.

Note that this repository assumes you have installed nvidia-docker2 and docker-compose.
Further, this docker environment assumes there exists a jupyter configuration json file before environment building.
So, please generate a configuration file like:
```shell
$ jupyter notebook --generate-config
$ jupyter notebook password
```
Then copy the file to `docker/gpu/`.

## How to run.
```shell
git submodule update course-v3
docker-compose run --service-ports jupyter-gpu
```
